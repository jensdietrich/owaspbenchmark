# OWASP Benchmark
- https://github.com/OWASP-Benchmark/BenchmarkJava
- version: commit/72258cb82f39e4e4b455e97f2aed2e5d37916bff

### Changes
Added 4 new classes
- `org.owasp.benchmark.fuzz.ApplicationPackageParamMissingException`
- `org.owasp.benchmark.fuzz.FeedbackInterceptor`
- `org.owasp.benchmark.fuzz.FeedbackService`
- `org.owasp.benchmark.fuzz.NotFoundException`


Added a new dependency in pom.xml. Changed port to 8080, removed SSL connection
```
<dependency>
    <groupId>nz.ac.wgtn.cornetto</groupId>
    <artifactId>cornetto-rt</artifactId>
    <version>2.0.2</version>
</dependency>
```

### Server deployment

1. checkout: https://bitbucket.org/jensdietrich/webfuzzer/
2. cd into webfuzz-rt and build with `mvn install`
3. check that the correct version referenced in the pom is installed
4. run ./runBenchmark.sh
5. the service should be available at http://localhost:8080/benchmark/
