package org.owasp.benchmark.fuzz;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import nz.ac.wgtn.cornetto.jee.rt.Constants;
import nz.ac.wgtn.cornetto.jee.rt.InvocationTracker;

public class FeedbackInterceptor implements Filter {

    protected FilterConfig config;

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }

    @Override
    public void doFilter(
            ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse myResponse = (HttpServletResponse) servletResponse;

        String path = httpRequest.getPathInfo();
        if (path == null) {
            path = "";
        }
        HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(myResponse);

        boolean isApplicationRequest = !path.contains(Constants.FUZZING_FEEDBACK_PATH_TOKEN);
        if (isApplicationRequest) {
            if (httpRequest.getHeader(Constants.TAINTED_INPUT_HEADER) != null) {
                InvocationTracker.DEFAULT.registerTaintStrings(
                        httpRequest.getHeader(Constants.TAINTED_INPUT_HEADER));
            }
            String ID = InvocationTracker.DEFAULT.startInvocationTracking();
            responseWrapper.addHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER, ID);
        }

        filterChain.doFilter(servletRequest, responseWrapper);

        isApplicationRequest = !path.contains(Constants.FUZZING_FEEDBACK_PATH_TOKEN);
        if (isApplicationRequest) {
            InvocationTracker.DEFAULT.finishInvocationTracking();
        }
    }

    @Override
    public void destroy() {}
}
