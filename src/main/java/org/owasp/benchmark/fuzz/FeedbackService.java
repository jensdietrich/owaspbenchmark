package org.owasp.benchmark.fuzz;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nz.ac.wgtn.cornetto.jee.rt.*;

@WebServlet(value = Constants.FUZZING_FEEDBACK_PATH_TOKEN + "/*")
public class FeedbackService extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getPathInfo().split("/")[1];
        String applicationpackages = request.getParameter("applicationpackages");
        if (id == null || id.length() == 0) {
            throw new NotFoundException(
                    "the id must be presented. e.g. __fuzzing_feedback/1"); // will generated 404
        }

        if (id.equals(Constants.SYSTEM_INVOCATIONS_TICKET)) {
            if (applicationpackages == null) {
                throw new ApplicationPackageParamMissingException(
                        "Request parameter applicationpackages is missing");
            }
            Set<String> invocations =
                    SystemInvocationTracker.getTrackedInvocations(applicationpackages);
            response.setContentType("text/plain");
            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);
            for (String invocation : invocations) {
                out.println(invocation);
            }
            PrintWriter writer = response.getWriter();
            writer.print(sw.toString());
            writer.flush();
        } else {
            Map<DataKind, List<Object>> tracked = InvocationTracker.DEFAULT.pickup(id);
            if (tracked == null) {
                throw new NotFoundException("No data found for id " + id); // will generated 404
            }

            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);
            Encoder.DEFAULT.encode(tracked, out);
            response.setContentType(Encoder.DEFAULT.getContentType());
            PrintWriter writer = response.getWriter();
            writer.println(sw.toString());
            writer.flush();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
